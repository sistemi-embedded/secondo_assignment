#ifndef __TASK__
#define __TASK__

#include "Arduino.h"

class Task {
  private:
    int myPeriod;
    int timeElapsed;
    bool active;
    bool periodic;
    bool completed;
  
  public:
      virtual void init(int period){
      myPeriod = period;  
      timeElapsed = 0;
      periodic = true;  
      active = true;
    }

  virtual void tick() = 0;

  bool updateAndCheckTime(int basePeriod){
    timeElapsed += basePeriod;
    if (timeElapsed >= myPeriod){
      timeElapsed = 0;
      return true;
    } else {
      return false; 
    }
  }

  void setCompleted(){
    completed = true;
    active = false;
  }

  bool isCompleted(){
    return completed;
  }

  bool isPeriodic(){
    return periodic;
  }

  bool isActive(){
    return active;
  }

  virtual void setPeriodic(bool periodic){
    this->periodic = periodic;
  }

  virtual void setActive(bool active){
    timeElapsed = 0;
    this->active = active;
  }
  
  int getTimeElapsed(){
    return this->timeElapsed;
  }
};

#endif
