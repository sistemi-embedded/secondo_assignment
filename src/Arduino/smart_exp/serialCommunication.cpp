#include "serialCommunication.h"
#include "MsgService.h"

serialCommunication::serialCommunication(){
  this->potMappedValue = 0;
}

void serialCommunication::notifyEnding(){
    MsgService.sendMsg("ENDING");
}

void serialCommunication::waitConfirm(){
  if(MsgService.isMsgAvailable()){
        Msg* msg = MsgService.receiveMsg();
        if(msg->getContent() == "OK" ){
            Serial.println("RECEIVED USER CONFIRM, RESTARTING");
            this->switchState(true, false, false);
        }
    }
}

void serialCommunication::switchState(bool ReadyBatteryTask, bool StartingErrorTask, bool RunningEndingTask){
    this->readyBatteryTask->setActive(ReadyBatteryTask);
    this->startingErrorTask->setActive(StartingErrorTask);
    this->runningEndingTask->setActive(RunningEndingTask);
}

void serialCommunication::setMacroTasks(Task* readyBatteryTask, Task* startingErrorTask, Task* runningEndingTask){
    this->readyBatteryTask = readyBatteryTask;
    this->startingErrorTask = startingErrorTask;
    this->runningEndingTask = runningEndingTask;

}

void serialCommunication::sendData(float vel, float temp, float acc){
    MsgService.sendMsg(String("VELOCITY(m/s):")+vel+String(" TIME(s):")+temp+String(" ACCELERATION(m/s^2):")+acc);
}

float serialCommunication::getPotMappedValue(){
    return this->potMappedValue;
}

void serialCommunication::setPotMappedValue(float value){
    this->potMappedValue = value;
}

void serialCommunication::reset(){
    serialCommunication* ser = new serialCommunication();
    ser->switchState(true, false, false);
}
