#include "RunningEndingTask.h"

RunningEndingTask::RunningEndingTask(serialCommunication* UserComm){
    this->UserComm = UserComm;
    
    this->blinking = new BlinkTask(L2_PIN);
    this->blinking->init(500);

    this->servo = new ServoMotorImpl(SERVO_PIN);
    this->sonarTask = new SonarTask(SONAR_TRIG_PIN, SONAR_ECHO_PIN, TEMPERATURE_PIN);
    this->CinematicTask = new cinematicTask(UserComm, this->servo);
    
    this->stopButton = new Button(BSTOP_PIN);
};

void RunningEndingTask::init(int period){
  Task::init(period);
  state = MVT_DETECTION;
};

void RunningEndingTask::setActive(bool active){
  Task::setActive(active);
  if (active){
    sent = false;
    state = MVT_DETECTION;
    startingTime = millis();
    
    digitalWrite(L2_PIN, HIGH);
    digitalWrite(L1_PIN, LOW);
    
    this->servo->on();
    this->lastPressing = 0;
    
    this->CinematicTask->init(UserComm->getPotMappedValue());
    this->sonarTask->init(UserComm->getPotMappedValue());
  } else {
    digitalWrite(L2_PIN, LOW);
  }
};

void RunningEndingTask::tick(){
  if(sent){
    switch(state){
        case MVT_DETECTION:
          if(this->stopButton->isPressed()){
            if(debouncing()){
              this->servo->off();
              state = DE_CONNECTING;
              UserComm->notifyEnding();
            }
          } else if((millis()-startingTime) >= MAX_TIME){
            this->servo->off();
            state = DE_CONNECTING;
            UserComm->notifyEnding();
          }
          if(sonarTask->updateAndCheckTime(20)){
            distance = sonarTask->getDistance();
            CinematicTask->action(distance);
          } 
        break;

        case DE_CONNECTING:
          if(this->blinking->updateAndCheckTime(20)){
            this->blinking->tick();
          }
          UserComm->waitConfirm();
        break;
    }
  } else {
    sendMessage();
  }
};

/*true if NO bouncing*/
bool RunningEndingTask::debouncing() {
    if(this->lastPressing == 0){
        lastPressing = millis();
        return true;
    } else if((millis()-lastPressing) > 100){
        lastPressing = millis();
        return true;
    } else {
      return false;
    }
}

void RunningEndingTask::sendMessage(){
  MsgService.sendMsg("RUNNING");
  delay(500);
  if(MsgService.isMsgAvailable()){
    Msg* msg = MsgService.receiveMsg();
    if(msg->getContent() == "OK GO" ){
      sent = true;
    }
  } else {
    sent = false;
  }
}
