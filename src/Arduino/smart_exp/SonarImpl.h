#ifndef __SONARIMPL__
#define __SONARIMPL__

#include "Sonar.h"

class SonarImpl : public Sonar{
  public:
    SonarImpl(int trigPin,int echoPin, float temperature);
    float getDistance();

  private:
    int trigPin;/*pin che registra*/
    int echoPin;/*pin che manda l'output*/
    float timeReg;/*frequanza di registrazione*/
    float temperature;/*serve per calcolare la distanza*/
};
#endif
