#ifndef __CINEMATICTASK__
#define __CINEMATICTASK__

#include "Task.h"
#include "serialCommunication.h"
#include "ServoMotorImpl.h"

class cinematicTask : public Task{
    public:
       cinematicTask(serialCommunication* UserComm, ServoMotor* servo);
       void init(int period);
       void action(float distance); 
       void tick(){};
       
    private:
       serialCommunication* UserComm;
       float getABS(float distance1, float distnce2);
       void resetVector();
       bool isReady();

      float timeElapsed;
      float shift;
    
      float acceleration;
      float velocity;

      float distances[2]={0,0};
      int distanceCounter;

      ServoMotor* servo;
};
#endif
