#include "serialCommunication.h"
#include "MsgService.h"
#include "Scheduler.h"
#include "ReadyBatteryTask.h"
#include "StartingErrorTask.h"
#include "RunningEndingTask.h"

/*
* PROJECT DEVELOPED BY
* BACCA RICCARDO --> riccardo.bacca@studio.unibo.it
* CAMPORESI STEFANO --> stefano.camporesi@studio.unibo.it
*/

Scheduler sched;

void setup() {
  sched.init(20);
  MsgService.init();

  serialCommunication* UserComm = new serialCommunication();
  
  Task* readyBatteryTask = new ReadyBatteryTask(UserComm);
  readyBatteryTask->init(20);
  sched.addTask(readyBatteryTask);

  Task* startingErrorTask = new StartingErrorTask(UserComm);
  startingErrorTask->init(20);
  startingErrorTask->setActive(false);
  sched.addTask(startingErrorTask);

  Task* runningEndingTask = new RunningEndingTask(UserComm);
  runningEndingTask->init(20);
  runningEndingTask->setActive(false);
  sched.addTask(runningEndingTask);
  
  UserComm->setMacroTasks(readyBatteryTask, startingErrorTask, runningEndingTask);

}

void loop() {
  sched.schedule();
}
