#ifndef __SONARTASK__
#define __SONARTASK__

#include "Task.h"
#include "SonarImpl.h"
#include "TempSensor.h"

class SonarTask : public Task{
    int trigPin;
    int echoPin;
    int distance;
    TempSensor* tempSensor;
    
    Sonar* sonar;

    public:
        SonarTask(int trigPin, int echoPin, int temperaturePin);
        void init(int period);
        float getDistance();
        void tick(){};
        bool isMoving();

};

#endif
