#ifndef __STARTINGERRORTASK__
#define __STARTINGERRORTASK__

class serialCommunication;
#include "serialCommunication.h"

#include "SonarTask.h"
#include "BlinkTask.h"
#include "Task.h"

#define ERROR_TIME 5000

class StartingErrorTask : public Task {
  
    public:
        StartingErrorTask(serialCommunication* UserComm);  
        void init(int period);  
        void setActive(bool active);
        void tick();

    private:
        enum { OBJ_DETECTION, ERROR } state;

        serialCommunication* UserComm;
        Task* blinking;
        SonarTask* sonarTask;
        
        long startingTime;
};


#endif
