#ifndef __READYBATTERYTASK__
#define __READYBATTERYTASK__

class serialCommunication;
#include "serialCommunication.h"

#include "Task.h"
#include "Button.h"
#include "potTask.h"
#include <avr/sleep.h>
#include <Arduino.h>

#define SLEEP_TIME 5000

class ReadyBatteryTask : public Task {
    
    public:
        ReadyBatteryTask(serialCommunication* UserComm);  
        void init(int period);  
        void setActive(bool active);
        void tick();

    private:
        enum { BUTTON_WAITING, ENERGYSAVING } state;
        static void reset();
        bool debouncing();
        serialCommunication* UserComm;
        Button* startButton;
        Task* PotTask;
        bool firstTime;
        long startingTime;
        long lastPressing;
};


#endif
