#include "cinematicTask.h"

cinematicTask :: cinematicTask(serialCommunication* UserComm, ServoMotor* servo){
    this->UserComm = UserComm;
    this->shift = 0;
    this->distanceCounter = -1;
    this->servo = servo;
}

void cinematicTask :: init(int period){
    Task::init(period);
    this->timeElapsed = period;
}

void cinematicTask :: action(float distance){
    distanceCounter++;
    distances[distanceCounter]=distance;
    
    if(isReady()){
      shift = getABS(distances[0], distances[1]);
      velocity = shift/(timeElapsed/1000);
      acceleration = ((velocity*velocity)-shift)/2;
      resetVector();
      
      this->servo->setPosition(velocity);
      
      UserComm->sendData(this->velocity, millis()/1000, this->acceleration);
    }
}

/*mi serve questo perchè l'ogetto può muoversi o verso il sensore o in direzione opposta */
float cinematicTask :: getABS(float distance1, float distance2){
    float tot = distance1-distance2;
    if(tot >= 0){
        return tot;
    }else{
        return -tot;
    }
}

void cinematicTask :: resetVector(){
    distances[0] = distances[1];
    distances[1] = 0;
    distanceCounter--;
}

bool cinematicTask::isReady(){
    if(distances[0] != 0 && distances[1] != 0){
        return true;
    }
    return false;
}
