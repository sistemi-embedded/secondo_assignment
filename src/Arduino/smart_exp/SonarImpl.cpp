#include "SonarImpl.h"
#include "Arduino.h"

SonarImpl :: SonarImpl(int trigPin, int echoPin, float temperature){
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  this->temperature = temperature;
  
  pinMode(trigPin,OUTPUT);
  pinMode(echoPin, INPUT);
}

float SonarImpl :: getDistance(){
    double vs = 331.45 + 0.62*temperature;
    digitalWrite(trigPin,LOW);
    delayMicroseconds(3);
    digitalWrite(trigPin,HIGH);
    delayMicroseconds(5);
    digitalWrite(trigPin,LOW);
    
    /* ricevi l’eco */
    long tUS = pulseInLong(echoPin, HIGH);
    double t = tUS / 1000.0 / 1000.0 / 2;
    double d = t*vs;
    return d;
}
