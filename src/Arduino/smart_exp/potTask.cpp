#include "potTask.h"

potTask:: potTask(int analogPin, serialCommunication* serialComm){
    this->analogPin = analogPin;
    this->serialComm = serialComm;
}

void potTask::init(int period){
    Task::init(period);
    pot = new Pot(analogPin);
    int value = 0;
}

void potTask::tick(){
    this->value = pot->pickValue();
    int tempValue = pot->mapValue(value, 0, 1023, 1, 9);// cosi ho 10 livelli
    switch(tempValue){
      case 1: serialComm->setPotMappedValue(20); break;
      case 2: serialComm->setPotMappedValue(30); break; 
      case 3: serialComm->setPotMappedValue(40); break;
      case 4: serialComm->setPotMappedValue(50); break;
      case 5: serialComm->setPotMappedValue(60); break;
      case 6: serialComm->setPotMappedValue(70); break;
      case 7: serialComm->setPotMappedValue(80); break;
      case 8: serialComm->setPotMappedValue(90); break;
      case 9: serialComm->setPotMappedValue(100); break;
    }
}
