#include "StartingErrorTask.h"

StartingErrorTask::StartingErrorTask(serialCommunication* UserComm){
    this->UserComm = UserComm;

    this->blinking = new BlinkTask(L1_PIN);
    this->blinking->init(500);

    this->sonarTask = new SonarTask(SONAR_TRIG_PIN, SONAR_ECHO_PIN, TEMPERATURE_PIN);
};

void StartingErrorTask::init(int period){
  Task::init(period);
};

void StartingErrorTask::setActive(bool active){
  Task::setActive(active);
  if (active){
    state = OBJ_DETECTION;
    this->sonarTask->init(UserComm->getPotMappedValue());
  }
};


void StartingErrorTask::tick(){
    switch(this->state){
        case OBJ_DETECTION:
          if(sonarTask->updateAndCheckTime(20)){
            if(sonarTask->isMoving()){
              Serial.println("MOVING, SWITCHING TO RUNNING");
              UserComm->switchState(false,false,true);
            } else {
              Serial.println("ERROR");
              startingTime = millis();
              this->state = ERROR;
            }
          }
        break;
        
        case ERROR:
            if((millis()-startingTime)>=ERROR_TIME){
                Serial.println("SWITCHING TO READY");
               UserComm->switchState(true,false,false);
            } else if(this->blinking->updateAndCheckTime(20)){
                this->blinking->tick();
            }
        break;
    }
};
