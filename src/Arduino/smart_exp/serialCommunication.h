#ifndef __SERIALCOMMUNICATION__
#define __SERIALCOMMUNICATION__

#include "ReadyBatteryTask.h"
#include "StartingErrorTask.h"
#include "RunningEndingTask.h"

#define L2_PIN 3
#define L1_PIN 2
#define PIR_PIN 7
#define BSTART_PIN 4
#define BSTOP_PIN 5
#define SONAR_TRIG_PIN 8
#define SONAR_ECHO_PIN 9
#define TEMPERATURE_PIN A1
#define POT_PIN A0
#define SERVO_PIN 10

class serialCommunication { 
  
    public:
      serialCommunication();
      
      void waitConfirm();
      void notifyEnding();
      
      void switchState(bool ReadyBatteryTask, bool StartingErrorTask, bool RunningEndingTask);
      void setMacroTasks(Task* readyBatteryTask, Task* startingErrorTask, Task* runningEndingTask);
      
      void sendData(float vel, float temp, float acc);
      float getPotMappedValue();
      void setPotMappedValue(float value);
      static void reset();
    private:
      float potMappedValue;
      Task* readyBatteryTask;
      Task* runningEndingTask;
      Task* startingErrorTask;
};

#endif
