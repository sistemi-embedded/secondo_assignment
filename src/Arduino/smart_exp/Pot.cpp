#include "Pot.h"
#include <Arduino.h>

Pot :: Pot(int pin){
    this -> pin = pin;
    pinMode(pin, INPUT);
}

int Pot::pickValue(){
    return analogRead(this -> pin);
}

int Pot::mapValue(long value, long fromLow, long fromHigh, long toLow, long toHigh){
    return map(value, fromLow, fromHigh, toLow, toHigh);
}