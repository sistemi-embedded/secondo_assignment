#include "TempSensor.h"
#include <Arduino.h>

TempSensor::TempSensor(int pin){
    this->pin = pin;
}

float TempSensor::getValue(){
    this->temperature = analogRead(this->pin);
}

float TempSensor::getValueInVolt(){
    return (this->temperature*VCC/1023);
}

float TempSensor::getValueInCelsius(){
    return (this->getValueInVolt()/0.01);
}