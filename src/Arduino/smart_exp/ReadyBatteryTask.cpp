#include "ReadyBatteryTask.h"
#include "EnableInterrupt.h"

ReadyBatteryTask::ReadyBatteryTask(serialCommunication* UserComm){
    this->UserComm = UserComm;
    this->PotTask = new potTask(POT_PIN, UserComm);
    this->PotTask->init(40);
    this->lastPressing = 0;
    this->startButton = new Button(BSTART_PIN);
};

void ReadyBatteryTask::init(int period){
  Task::init(period);
  state = BUTTON_WAITING;
  digitalWrite(L1_PIN, HIGH);
  this->firstTime = true;
};

void ReadyBatteryTask::setActive(bool active){
  Task::setActive(active);
  if (active){
    state = BUTTON_WAITING;
    digitalWrite(L1_PIN, HIGH);
    this->firstTime = true;
    this->lastPressing = 0;
  }
};

void ReadyBatteryTask::tick(){
  this->PotTask->tick();
    switch(this->state){
        case BUTTON_WAITING:
            if(firstTime){
                this->startingTime = millis();   
                firstTime = false;
            }
            if((millis()-startingTime)>=SLEEP_TIME){
                Serial.println("energy saving");
                digitalWrite(L1_PIN, LOW);
                state = ENERGYSAVING;    
            } else if(this->startButton->isPressed()){      
                if(debouncing()){
                    this->PotTask->tick();
                    Serial.println(String("REGISTRATION FREQUENCY: ")+UserComm->getPotMappedValue()/1000+ String(" seconds"));     
                    UserComm->switchState(false, true, false);
                }
            }
        break;

        case ENERGYSAVING:
            set_sleep_mode(SLEEP_MODE_PWR_DOWN);
            sleep_enable();  
            enableInterrupt(PIR_PIN, reset, RISING); 
            sleep_mode();
            sleep_disable();
            disableInterrupt(PIR_PIN);
        break;
    }
};

void ReadyBatteryTask::reset(){
    Serial.println("AWAKE");
    Serial.flush();
    serialCommunication::reset();
};

/*true if NO bouncing*/
bool ReadyBatteryTask::debouncing() {
    if(this->lastPressing == 0){
        lastPressing = millis();
        return true;
    } else if((millis()-lastPressing) > 100){
        lastPressing = millis();
        return true;
    } else {
      return false;
    }
}
