#ifndef __RUNNINGENDINGTASK__
#define __RUNNINGENDINGTASK__

class serialCommunication;
#include "serialCommunication.h"

class cinematicTask;
#include "cinematicTask.h"

#include "SonarTask.h"
#include "Task.h"
#include "Button.h"
#include <Arduino.h>
#include "MsgService.h"
#include "ServoMotorImpl.h"

#define MAX_TIME 20000

class RunningEndingTask : public Task {
  
    public:
        RunningEndingTask(serialCommunication* UserComm);  
        void init(int period);  
        void setActive(bool active);
        void tick();
        bool sent;

    private:
        enum { MVT_DETECTION, DE_CONNECTING } state;
        serialCommunication* UserComm;
        
        SonarTask* sonarTask;
        cinematicTask* CinematicTask;
        Task* blinking;
        
        bool debouncing();
        
        void sendMessage();
        void sendEnding();
          
        Button* stopButton;
        
        float distance;
        long startingTime;
        long lastPressing;

        ServoMotor* servo;
};


#endif
