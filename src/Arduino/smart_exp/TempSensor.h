#ifndef __TEMPSENSOR__
#define __TEMPSENSOR__

#define VCC ((float)5)

class TempSensor{
    private:
        int pin;
        int temperature;

    public:
        TempSensor(int pin);
        float getValueInVolt();
        float getValueInCelsius();
        float getValue();

};

#endif