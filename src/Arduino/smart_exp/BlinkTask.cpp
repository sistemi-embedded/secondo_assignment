#include "BlinkTask.h"

BlinkTask::BlinkTask(int pin){
  this->pin = pin;    
}
  
void BlinkTask::init(int period){
  Task::init(period);
  led = new Led(pin); 
  LedState = OFF;    
}
  
void BlinkTask::tick(){
  switch (LedState){
    case OFF:
      led->switchOn();
      LedState = ON; 
      break;
    case ON:
      led->switchOff();
      LedState = OFF;
      break;
  }
}