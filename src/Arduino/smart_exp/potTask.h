#ifndef __POTTASK__
#define __POTTASK__

#include "Task.h"
#include "Pot.h"
#include "serialCommunication.h"

class potTask : public Task {
    int analogPin;
    Pot* pot;
    int value;
    serialCommunication* serialComm;

    public:
        potTask(int analogPin, serialCommunication* serialComm);
        void init(int period);
        void tick();
};

#endif
