#ifndef __POT__
#define __POT__

class Pot {
    public:
        Pot(int pin);
        int mapValue(long value, long fromLow, long fromHigh, long toLow, long toHigh);
        int pickValue();
    private:
        int pin;
};

#endif