#include "SonarTask.h"
#include "MsgService.h"
#include <Arduino.h>

SonarTask :: SonarTask(int trigPin, int echoPin, int temperaturePin){
    this->trigPin = trigPin;
    this->echoPin = echoPin;
    this->tempSensor = new TempSensor(temperaturePin);
}

/*period recived by potentiometer*/
void SonarTask :: init(int period){
    Task::init(period);
    sonar = new SonarImpl(trigPin, echoPin, this->tempSensor->getValueInCelsius());
}

float SonarTask :: getDistance(){
   return sonar->getDistance();
}

bool SonarTask::isMoving(){
    float checkDistance[5];
    Serial.println("DETECTING OBJECT..");
    for(int i=0; i<5; i++){
      checkDistance[i] = sonar->getDistance();
      delay(1000);
    }
    if(checkDistance[4]==checkDistance[3]){
      return false;
    } else {
      return true;
    }
}
