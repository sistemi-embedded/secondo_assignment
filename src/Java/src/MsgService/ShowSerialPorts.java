package MsgService;
import jssc.*;

public class ShowSerialPorts {

	public String getPortName() {
		/* detect serial ports */
		String[] portNames = SerialPortList.getPortNames();
		return portNames[0];
	}

}
