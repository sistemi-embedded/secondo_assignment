package Viewer;

import java.util.*;
import javax.swing.JOptionPane;

public class Utils {
    private Map<Float, Float> arduinoData = new HashMap<>();
    private LineChartEx graph = new LineChartEx();
    private boolean createGraph = false;

    public boolean getUserConfirm(){
        int input = JOptionPane.showConfirmDialog(null, 
        "CLICK OK TO END EXPERIMENT", "USER CONFIRM", JOptionPane.DEFAULT_OPTION);
        if(input == 0){
            this.graph.resetGraph();
            this.createGraph = false;
            this.drawGraphic(this.createGraph);
            this.arduinoData.clear();
            return true;
        }
        return false;
    }

	public void receiveData(String msg) throws InterruptedException {
        if(msg.startsWith("VEL")){
            String[] strArray = msg.split(" ");
            String vel = strArray[0];
            String tempo = strArray[1];
            String[] velValue = vel.split(":");
            String[] tempValue = tempo.split(":");
            this.arduinoData.put(Float.parseFloat(tempValue[1])/1000,Float.parseFloat(velValue[1]));
            this.graph.addPoint(this.arduinoData);
            if(!this.createGraph){
                this.createGraph = true;
                this.drawGraphic(this.createGraph);  
            }
        }
    }
        
    
    private void drawGraphic(boolean b){
        this.graph.setVisible(b);
    }
}
