package Viewer;

import MsgService.*;

/*
* PROJECT DEVELOPED BY
* BACCA RICCARDO --> riccardo.bacca@studio.unibo.it
* CAMPORESI STEFANO --> stefano.camporesi@studio.unibo.it
*/
public class Viewer {
	public static void main(String[] args) throws Exception {
		Utils utils = new Utils();
		ShowSerialPorts port = new ShowSerialPorts();
		CommChannel channel = new SerialCommChannel(port.getPortName(),9600);	
		
		System.out.println("Waiting Arduino for rebooting...");		
		Thread.sleep(4000);
		System.out.println("Ready.");

        while(true){
			if(channel.isMsgAvailable()){
				String msg = channel.receiveMsg();
				System.out.println("Received: " + msg);
				if(msg.equals("RUNNING")){	
					channel.sendMsg("OK GO");
				} else if(msg.equals("ENDING")){
					if(utils.getUserConfirm()){
						channel.sendMsg("OK");
					}
				} else {
					utils.receiveData(msg);
				}
			} 
		}
	}
}