Progetto realizzato da:
-Bacca Riccardo --> riccardo.bacca@studio.unibo.it
-Camporesi Stefano --> stefano.camporesi@studio.unibo.it

Nel progetto sono utilizzate le seguenti librerie esterne:
-Arduino:
    *MsgService.h
    *EnableInterrupt.h
    *ServoTimer2.h
-Java:
    *JFreeChart
    
Il progetto è stato sviluppato utilizzando come IDE principale ed unico Visual Studio Code.

La versione di Java utilizzata è la JDK 11.0.6
La versione di C++ utilizzata è la cppStandard 17

